
%%% Local Variables:
%%% mode: latex
%%% TeX-master: t
%%% End:

%%% Local Variables:
%%% mode: latex
%%% TeX-master: t	
%%% End:
\documentclass[acmsmall,review,anonymous]{acmart}\settopmatter{printfolios=true,printccs=false,printacmref=false}
\citestyle{acmauthoryear}   %% For author/year citations
\setcopyright{none}

% \makeatletter
% \providecommand{\@LN}[2]{}
% \makeatother
% use Times
% \usepackage{times}
% For figures

\usepackage{graphicx} % more modern
%\usepackage{epsfig} % less modern
%\usepackage{subfigure}
\usepackage{caption}
\usepackage[labelformat=simple]{subcaption}
\renewcommand\thesubfigure{(\alph{subfigure})}
% For citations
\usepackage{natbib}
%\usepackage{cite}

% For algorithms
\usepackage[ruled]{algorithm2e} % For algorithms
\renewcommand{\algorithmcfname}{ALGORITHM}
\SetAlFnt{\small}
\SetAlCapFnt{\small}
\SetAlCapNameFnt{\small}
\SetAlCapHSkip{0pt}
\IncMargin{-\parindent}


\usepackage{hyperref}

\hypersetup{
  colorlinks   = true, %Colours links instead of ugly boxes
  urlcolor     = blue, %Colour for external hyperlinks
  linkcolor    = blue, %Colour of internal links
  citecolor   = red %Colour of citations
}

% Packages hyperref and algorithmic misbehave sometimes.  We can fix
% this with the following command.


\usepackage{cc,lambda}
\usepackage[noshare]{vlc}
\usepackage{proof}
\usepackage{pdfpages}
\usepackage{multirow}
\usepackage{xargs}

\usepackage{mathpartir}
\usepackage{mathtools}
\usepackage{xcolor}
\usepackage{calc}
\usepackage[labelformat=simple]{subcaption}
\renewcommand\thesubfigure{(\alph{subfigure})}
\usepackage{paralist}
\usepackage{tikz}
\usepackage{booktabs}
\usepackage{pifont}
\usepackage[inline]{enumitem}
\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
%\usepackage{minted}
\usepackage{scalerel,amssymb,amsmath}
\usepackage{stmaryrd}
\usepackage{wrapfig}

\input{errd.tex}

% make things look nicer
\sloppy
\sloppypar

\newcommand{\cL}{{\cal L}}

\renewcommand{\progind}{0pt}

\DefineVerbatimEnvironment{programS}{Verbatim}
   {baselinestretch=1.0,xleftmargin=\parindent,fontsize=\progfontsize,
   frame=lines, framesep=0mm, framerule=0mm,
   commandchars=\\\{\},samepage=true}

\newcommand{\tightbox}[1]{{\setlength{\fboxsep}{0pt}\fbox{\vphantom{(y}#1}}}
\newcommand{\gbkg}[1]{{\setlength{\fboxsep}{0pt}\colorbox{lightgray}{\vphantom{[y}#1}}}
\newcommand{\gbkgray}[1]{{\setlength{\fboxsep}{0pt}\underline{\vphantom{[y}#1}}}
% \newcommand{\gbkgray}[1]{{\setlength{\fboxsep}{0pt}\colorbox{lightgray}{\vphantom{[y}#1}}}

% For the Type System Section
\newcommand*{\cross}{%
% \mathbin{%
    \text{%
      \raise .5ex\hbox{%
        \rlap{\vrule height.2pt depth.2pt width .75ex}%
        \hbox to .75ex{\hss\vrule height .75ex depth .75ex\hss}%
      }%
    }%  
}

\def\msquare{\mathord{\scalerel*{\Box}{gX}}}
\DeclarePairedDelimiter\ceil{\lceil}{\rceil}
\DeclarePairedDelimiter\floor{\lfloor}{\rfloor}


%\hypersetup{draft}


\input{macros}
\begin{document}

%\title{Close the Loophole, Detecting and Fixing Type Unsoundness}
%\title{\betterTitle}
\title{Get Active}
\subtitle{Learning Gradual Typing Performance}
\begin{abstract}
  The performance of programs written in a gradually typed language can often be unpredictable.
\end{abstract}
\section{Introduction}
\label{sec:Intro}

\section{Background}
In order for the unpredictable slowdowns that were described in Section~\ref{sec:Intro} to be present, we assume that the gradually typed
language under considertation adds runtime type checks known as \emph{casts} to protect the typing invariants
specified by the program. This means that optionally typed languages (or languages using the erasure
approach as coined by \citet{Greenman:2018:STS}) such as TypeScript do not illustrate such an issue, while
languages like Reticulated Python \citep{Vitousek:2014:DLS}, Racket/Typed Racket \citep{Tobin-Hochstadt:2008:DIT}, and Grift (PLDI citation needed)
all can have unpredicatable slowdowns in performance. One might consider this a reason to abandon gradual language designs that enforce type invariants
at runtime, but a study by \citet{Wilson:2018:BGT} shows that programmers often expected the
behavior of programs to emulate those done by guarded gradual typing (aka the higher-order approach
\citep{Greenman:2018:STS}). Unfortunately, languages that use the guarded approach to gradual typing incur the largest and most unpredictable performance
swings.

To illustrate \emph{why} adding these casts can have such terrible slowdown, we will consider the following Reticulated Python program adapted from
\cite{Herman:2010:SGT}. This program can easily be translated to other languages like Grift and Racket.
%
\begin{figure}[t]
\begin{program}
  def even(x, k1):
    return k1(True) if x == 0 else odd(x-1, k1)
  
  def odd(x, k2:Function([Bool], Bool)):
    return k2(False) if x == 0 else even(x-1, k2)
  \end{program}
  \label{fig:even-odd}
  \caption{A pair of mutually recursive functions written in CPS that determine whether a number is even or odd.}
\end{figure}
%
In the program present in Figure~\ref{fig:even-odd}, notice that there is a type annotation present on \prog{k2}, but not a type annotation present on
\prog{k1}. This means that \prog{k1} has the type \dyn\ and is dynamically typed, while \prog{k2} has type \fun{\Bool}{\Bool} and is statically typed. Note
that the call \prog{odd(x-1, k1)} passes the dynamically typed \prog{k1} as an argument that flows into the statically typed parameter \prog{k2}. This means
that the value inside of \prog{k1} needs to be checked to ensure that it is indeed a function of type \fun{\Bool}{\Bool}.
\bibliographystyle{ACM-Reference-Format}
\bibliography{paper}
\end{document}