# Active Learning for Gradual Typing Performance

## Building Environment

* numpy 1.14.2
* scipy 1.0.0
* scikit-learn 0.19.1
* pandas 0.22.0
* pickle 0.7.4
* matplotlib 2.1.2
* alipy 1.2.1
* torch 
* torch scatter 2.0.3
* torch sparse 0.5.1
* torch geometric 1.4.2
* CUDA 10.1.168

For GPU support, 
we develop the codebase upon the [PyTorch](https://pytorch.org/get-started/locally/) package.
Also, to simply the GCN implementation, the [PyTorch Geometric](https://pytorch-geometric.readthedocs.io/en/latest/notes/installation.html)
package needs to be installed.  


## Usage  

* `smartQuery.py`: an active learning method is implemented, which respects the graph topological structure via employing GCN node embedding.  
* `main.py`: execute experiments with two algorithms, namely *Max Uncertain Query-By-Committee* (QBC) and *Random Sampling* (RS), and dump the results.  
* `benchmarks.py`: test the dataset quality by running the *passive learners* (abundant labeled instances are available beforehand for training). This illustrates the performance upper bound that we can expect from using the active learning strategy.  
* `plots.plotter.py`: generate figures with the results obtained from `main.py`.  
* `plots.non_linear.py`: generate figures that summary the feature-runtime distribution, *etc.*  
* `method.random_sampling.py`: implement an RS-based active learning algorithm. One *Multi-Layer Perceptron* (MLP) is constructed as learner, which is trained with a small amount of labeled instances. At each iteration, the learner randomly queries several labeled instances, and is updated with those queried instances.  
* `method.max_uncertain_QBC.py`: implement a QBC-based active learning algorithm. N (controlled by 'COMMITTEE_SIZE') numbers of MLPs are constructed to form the 'committee', with each MLP being a committee member (learner). Each learner is trained with a 30% instance subset randomly sampled from the given labeled instances. At each iteration, the committee members predict several unlabeled instances among which that have the highest variance are queried.  
* `method.least_confident_classification.py`: implement an classification-focused active learning algorithm, named [SPAL][2]. The implementation is built upon [the `alipy` package][1]. Please refer to the links for more details.  

## Example Results

**CORA**:  
![alt text][CORA]  

**RayTrace**:  
![alt text][raytrace]  
  
**Metero**:  
![alt text][meteor]  
  
**NBody**  
![alt text][nbody]  




[1]: http://parnec.nuaa.edu.cn/huangsj/alipy/
[2]: https://www.aaai.org/ojs/index.php/AAAI/article/view/4445
[CORA]: /plots/Cora_result.jpg "CORA"
[raytrace]: /plots/RayTrace_result.jpg "RayTrace"
[meteor]: /plots/Meteor_result.jpg "Metero"
[nbody]: /plots/NBody_result.jpg "NBody"